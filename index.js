const express = require('express');
const path = require('path');
const app = express();
const port = 3001;

const homeRouter = require('./src/routes/home');

app.set('views', [path.join(__dirname),path.join('src'), path.join('src/home-views'),])
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({extended:false}));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', homeRouter);

app.listen(port, ()=>{
    console.log(`App is listening on http://localhost:${port}`)
});