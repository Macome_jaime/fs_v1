const express = require('express');
const router = express.Router();
const HomeController = require('../controller/homeController')
const homeController = new HomeController();

router.get('/', (req,res)=> homeController.home(req, res));

module.exports = router;