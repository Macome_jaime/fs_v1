
class controller {

    home(req, res) {
        let date = new Date();
        let time = String(date.getHours()).padStart(2,'0') + ':'+ String(date.getMinutes()).padStart(2, '0') + ':' + String(date.getSeconds()).padStart(2,'0');
        date = date.getFullYear() + '/' + String(date.getMonth() + 1).padStart(2, '0') + '/'+ String(date.getDate()).padStart(2, '0');
        res.render('home.ejs',{date:date, time:time});
    }
}

module.exports = controller;